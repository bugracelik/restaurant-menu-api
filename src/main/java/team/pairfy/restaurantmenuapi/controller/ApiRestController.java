package team.pairfy.restaurantmenuapi.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import team.pairfy.restaurantmenuapi.model.Cookie;
import team.pairfy.restaurantmenuapi.services.ApiServices;

@RestController
public class ApiRestController {

    ApiServices apiServices = new ApiServices();

    @GetMapping("kurabiye")
    public Cookie getMyCookie(){
       return apiServices.getMyCookie();
    }

    @GetMapping("sicakkahve")
    public void getMyHotCoffe(){

    }

    @GetMapping("sogukkahve")
    public void getMyColdCoffe(){

    }

    @GetMapping("demleme")
    public void getMyFilterCoffe(){

    }

    @GetMapping("cay")
    public void getMyTea(){

    }

    @GetMapping("ok")
    public void ok(){

    }


    
}
