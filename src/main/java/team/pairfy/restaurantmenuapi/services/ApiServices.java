package team.pairfy.restaurantmenuapi.services;

import team.pairfy.restaurantmenuapi.model.Cookie;
import team.pairfy.restaurantmenuapi.repositories.ApiRepositories;

public class ApiServices {

    ApiRepositories apiRepositories = new ApiRepositories();

    public Cookie getMyCookie() {
        return apiRepositories.getMyCookie();
    }
}
